/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht12proxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author aleks
 */
public class ProxyExample {

   /**
    * Test method
    */
   public static void main(final String[] arguments) {
       
        
        Scanner sc = new Scanner(System.in);
        final Image image3 = new ProxyImage("HiRes_10MB_Photo3");
        final Image image4 = new ProxyImage("HiRes_10MB_Photo4");
        final Image image5 = new ProxyImage("HiRes_10MB_Photo5");
        final Image image6 = new ProxyImage("HiRes_10MB_Photo6");
        final Image image7 = new ProxyImage("HiRes_10MB_Photo7");
        List<ProxyImage> ProxyList = new ArrayList<ProxyImage>();
        ProxyList.add((ProxyImage) image3);
        ProxyList.add((ProxyImage) image4);
        ProxyList.add((ProxyImage) image5);
        ProxyList.add((ProxyImage) image6);
        ProxyList.add((ProxyImage) image7);
        for (Image item : ProxyList) {
            item.showData();
        }
        int lk = 0; // listan kohta
        int valinta = sc.nextInt();
        System.out.println("Selaa valokuvakansiota 1 eteenpäin 2 takaisin. 3 Lopettaa");
        while(valinta != 3){
        switch (valinta) {
            case 1: 
                Image nauta = ProxyList.get(lk);
                    nauta.displayImage();
                    nauta.showData();
                    lk++;
                    break;
            case 2: 
                Image nauta2 = ProxyList.get(lk);
                    nauta2.displayImage();
                    nauta2.showData();
                    lk--;
                    break;
            default:
                System.out.println("Lista tyhjä");
        }
        }
        
        final Image image1 = new ProxyImage("HiRes_10MB_Photo1");
        final Image image2 = new ProxyImage("HiRes_10MB_Photo2");

        image1.displayImage(); // loading necessary
        image1.displayImage(); // loading unnecessary
        image2.displayImage(); // loading necessary
        image2.displayImage(); // loading unnecessary
        image1.displayImage(); // loading unnecessary
    }
}